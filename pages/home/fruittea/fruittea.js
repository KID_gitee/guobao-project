// pages/home/fruittea/fruittea.js
Page({
 
    /**
     * 页面的初始数据
     */
    data: {
      currentIndex:0,//左边菜单的当前显示值
      toView: 'menu-0', //滚动到某个菜单元素
      foodListHeights:[],//菜单对应右边商品的元素高度列表
      scrollTop:0,//右边商品滚动条滚动到哪
      goods: [
        {
          "name": "热销榜",
          "type": -1,
          "foods": [
            {
              "info": "门店销量第一名",
            },                             
          ]
        },
        {
          "name": "果茶",
          "type": 2,
          "foods": []
        }
      ]
    }, 
    /**
     * 跳到某一个菜单元素
     */
    scrollToMenu: function (e){ 
      var that=this;
      let current = e.currentTarget.dataset.current; 
      let toViewString = 'menu-' + (current > 5 ? current - 5 : 0); 
      that.setData({
        currentIndex: current,
        toView: toViewString,
        scrollTop: that.data.foodListHeights[current]
      });  
    },
    /**
     * 监听商品滚动事件
     */
    scrollFoods: function (e) {
      var that=this;
      let currentY=e.detail.scrollTop;
      for (let i = 0; i < that.data.foodListHeights.length - 1; i++) {
        let heightBottom = that.data.foodListHeights[i];
        let heightTop = that.data.foodListHeights[i + 1];
        //对滑动后currentY值不足的情况进行修正
        let diff = Math.abs(currentY - heightTop); 
        //判断currentY当前所在的区间
        if (currentY < heightTop && currentY >= heightBottom) {
          let toViewString = 'menu-' + (i > 5 ? i - 5 : 0);
          that.setData({
            currentIndex: i,
            toView: toViewString
          }); 
        }
      }
    }, 
    
    /**
     * 生命周期函数--监听页面加载后执行事件
     */
    onLoad: function (options) {
      wx.request({
        url: getApp().globalData.server + "/index.php/Home/GuoBao/FindFruitGoods",
        header: {"Content-Type": "application/x-www-form-urlencoded" },
        method: "post",
        data: {
          goods_type: '水果茶'
        },
        success:function(res){
          // console.log(res.data.data)
          // console.log(that.data.goods)
          var len = res.data.data.length
          var foods = res.data.data.splice(1,len-1)
          console.log(foods)
          that.setData({
            'goods[0].foods[0].goods_name': res.data.data[0].goods_name,
            'goods[0].foods[0].goods_img': res.data.data[0].goods_img,
            'goods[0].foods[0].goods_price': res.data.data[0].goods_price,
            'goods[0].foods[0].goods_sellcount': res.data.data[0].goods_sellcount,
            'goods[1].foods': foods
          })
        },
        fail:function(){

        }
      })
      const that = this; 
      let height = 0;
      const _foodListHeights=[];
      _foodListHeights.push(height);  
      const query = wx.createSelectorQuery();
      query.select('.foods-wrapper').boundingClientRect();
      query.selectAll('.food-list-hook').boundingClientRect();  
      query.exec(function (res) { 
        //height = height - res[0].height; 
        for(let i=0;i<res[1].length;i++){
          height += res[1][i].height;
          _foodListHeights.push(height); 
        } 
        that.setData({
          foodListHeights: _foodListHeights
        });
      }) 
    }
  })