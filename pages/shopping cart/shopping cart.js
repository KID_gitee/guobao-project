// pages/shopping cart/shopping cart.js

const app = getApp()
Page({
  data: {
    dataSource: [{
      cart_id: 1, //商品ID
      name: '苹果（500g)', //商品名称
      quantity: 1, //商品数量
      option: [{
          value: '1个/袋'
        },
        {
          value: '特辣； 1个/袋'
        }
      ],
      image: '/pages/image/apple.png', //商品图片
      price: 9.8, //商品价格
      checks: false //是否选中
    }], //购物车数据
    total_all_price: 0, //商品的总价格
    all_shop: 0, //所选商品的id
    shop_quantity: 0, //所选商品的数量
    good_one: [], //单个商品的数据
    checked: false, //是否为全选
    is_shop: false, //是否有商品
    shopData: null
  },
  onLoad: function(options) {
 
  },
  onReady: function() {
    // 生命周期函数--监听页面初次渲染完成
  },
  onShow: function() {
    // 生命周期函数--监听页面显示
    var that = this;
    that.setData({
      total_all_price: 0,
      checked: false
    })
    var user_data = wx.getStorageSync('user_data');
    // if (user_data == '') {
    //   wx.showModal({
    //     title: '错误提示',
    //     content: '请登录',
    //     success: function (sm) {
    //       if (sm.confirm) {
    //         wx.switchTab({
    //           url: '../my/my',
    //         })
    //       } else {
    //         wx.switchTab({
    //           url: '../index/index',
    //         })
    //       }
    //     }
    //   })
    // } else {
    //   wx.request({
    //     url: url,
    //     data: {
    //       'uid': user_data.uid
    //     },
    //     method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
    //     // header: {}, // 设置请求的 header
    //     header: {
    //       'content-type': 'application/json'
    //     },
    //     success: function (res) {
    //       if (res.data.goods.length == 0) {
    //         that.setData({
    //           is_shop: true
    //         })
    //       } else {
    //         that.setData({
    //           is_shop: false
    //         })
    //       }
    //       that.setData({
    //         dataSource: res.data.goods,
    //       })
    //     },
    //   })
    // }
  },
  onHide: function() {
    // 生命周期函数--监听页面隐藏
  },
  onUnload: function() {
    // 生命周期函数--监听页面卸载
  },
  onPullDownRefresh: function() {
    // 页面相关事件处理函数--监听用户下拉动作
  },
  onReachBottom: function() {
    // 页面上拉触底事件的处理函数
  },
  onShareAppMessage: function() {
    // 用户点击右上角分享
    return {
      title: 'title', // 分享标题
      desc: 'desc', // 分享描述
      path: 'path' // 分享路径
    }
  },
  // 添加按钮被点击
  addButtonClick: function(tap) {
    var that = this;
    var user_data = wx.getStorageSync('user_data'); //获取缓存里面的用户信息
    const index = parseInt(tap.currentTarget.id); //获取当前的商品的索引值
    let dataSource = that.data.dataSource; //购物车所有的商品数据
    let quantity = dataSource[index].quantity; //获取购买数量
    quantity = quantity + 1; //将购买数量 +1
    dataSource[index].quantity = quantity; //更改当前商品的数量
    that.setData({
      dataSource: dataSource //更新商品数据
    });
    // wx.request({
    //   url: url,
    //   data: {                                         //发送给后端的参数
    //     "cart_id": dataSource[index].cart_id,
    //     "q": dataSource[index].quantity,
    //     "id": dataSource[index].goods_id,
    //     "uid": user_data.uid
    //   },
    //   method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
    //   // header: {}, // 设置请求的 header
    //   header: {
    //     'content-type': 'application/json'
    //   },
    //   success: function (res) {
 
    //   },
    // })
    that.getTotalPrice(); //重新计算总价格
  },
  // 减少按钮被点击
  reduceButtonClick: function(tap) {
    var that = this;
    var user_data = wx.getStorageSync('user_data');
    const index = parseInt(tap.currentTarget.id);
    let dataSource = that.data.dataSource[index];
    let quantity = dataSource.quantity; //获取购买数量
    if (quantity == 1) { //判断是否等于1
      wx.showModal({
        title: '提示',
        content: '确定将  ' + dataSource.name + '  移出购物车吗？',
        success: function(sm) {
          if (sm.confirm) {
            var tempData = that.data.dataSource; //所有商品数据
            tempData.splice(index, 1); //从当前索引值开始删除1项数据
            that.setData({
              dataSource: tempData //更新数据
            })
            // wx.request({
            //   url: url,                             //发送删除请求
            //   data: {
            //     "cart_id": dataSource.cart_id,
            //     "uid": user_data.uid
            //   },
            //   method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            //   // header: {}, // 设置请求的 header
            //   header: {
            //     'content-type': 'application/json'
            //   },
            //   success: function (res) {
            //     that.onShow()                       //删除成功之后刷新当前页面
            //   },
            // })
          } else if (sm.cancel) {
            return false;
          }
        }
      })
    } else { //如果商品数量不等于 1                                 
      var dataSource_2 = that.data.dataSource;
      var quantity2 = quantity - 1;
      dataSource_2[index].quantity = quantity2;
      this.setData({
        dataSource: dataSource_2
      });
      // wx.request({
      //   url: url,
      //   data: {
      //     "cart_id": dataSource_2[index].cart_id,
      //     "q": dataSource_2[index].quantity,
      //     "id": dataSource_2[index].goods_id,
      //     "uid": user_data.uid
      //   },
      //   method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      //   // header: {}, // 设置请求的 header
      //   header: {
      //     'content-type': 'application/json'
      //   },
      //   success: function (res) {
 
      //   },
      // })
    }
    that.getTotalPrice(); //重新计算总价格
  },
  /*checkbox 选中或未选中都会触发该事件*/
  checkboxChange: function(e) {
    var that = this;
    that.setData({
      all_shop: e.detail.value,
    })
  },
  //删除单个商品
  deleteGoods(e) {
    const index = e.currentTarget.dataset.index;
    const cartId = e.currentTarget.dataset.cart_id;
    let carts = this.data.dataSource;
    wx.showModal({
      title: '温馨提示',
      content: '确定删除当前商品吗？',
      confirmColor: "#f00",
      success: (res) => {
        if (res.confirm) {
          wx.showLoading({
            title: '加载中'
          });
          carts.splice(index, 1);
          this.updateCarts(carts);
          //更新总价
          this.getTotalPrice();
          wx.hideLoading();
          // wx.request({
          //   url: app.baseURL + 'Car&a=delete_cart_item',
          //   data: {
          //     cart_id: cartId,
          //     user_id: app.globalData.userId
          //   },
          //   success: (res) => {
          //     if (res.statusCode == 200) {
          //       if (res.data.status == 1) {
          //         //购物车商品数量，数字类型
          //         let num = res.data.car_num;
          //         //将购物车商品数量存入本地并更新
          //         // wx.setStorageSync("car_num", num + '0');
          //         // wx.removeTabBarBadge(2);
          //         // app.setBadge(2, num + '');
          //         //更新数据
          //         carts.splice(index, 1);
          //         this.updateCarts(carts);
          //         //更新总价
          //         this.getTotalPrice();
          //         wx.hideLoading();
          //       }
          //     }
          //   }
          // });
        }
      }
    })
  },
  //更新数据函数
  updateCarts(data) {
    this.setData({
      dataSource: data
    })
  },
  /*点击结算 */
  toBuy: function(tap) {
    var that = this;
    let good = this.data.dataSource; 
    console.log(good)
    let user_info = wx.getStorageSync('user_data')
    if (that.data.total_all_price == 0) { //判断是否选择了商品，这里我是判断总价格
      wx.showToast({
        title: '请选择商品',
      })
    } else {
      this.data.good_one = []; //重置数组
      for (var i = 0; i < this.data.dataSource.length; i++) { //循环购物车中的商品
        if (good[i].checks == true) {
          var good_one1 = [good[i].cart_id, good[i].quantity];
          this.data.good_one.push(good_one1) //将数据添加到数组里边
        }
      }
      // wx.request({
      //   url: url,                                      //发送结算请求
      //   data: {
              //以json格式传输给服务器至数据库储存
      //     'data': JSON.stringify(this.data.good_one),    
      //     'uid': user_info.uid
      //   },
      //   method: 'POST',
      //   success: function (res) {
      //     var res_1 = JSON.stringify(res.data)         //将返回的数据格式化再作为参数跳转到预订单页面
      //     wx.navigateTo({
      //       url: '../buy/buy?data=' + res_1,
      //       success: function (res) {
      //       }
      //     })
      //   }
      // })
    }
  },
  /**点击全选 */
  setChecked: function() {
    let checked = this.data.checked; //是否为全选状态
    checked = !checked; //改变状态
    let dataSource = this.data.dataSource;
    for (let i = 0; i < dataSource.length; i++) {
      dataSource[i].checks = checked; // 改变所有商品状态
    }
    this.setData({
      checked: checked, //更新全选状态
      dataSource: dataSource //更新所有商品的状态
    });
    this.getTotalPrice(); //重新获取总价格
  },
  /**点击单个多选框 */
  allCheckbox: function(tap) {
    var a = 0 //设置初始总价格
    var index = tap.currentTarget.dataset.index //获取索引值
    let good = this.data.dataSource //获取购物车列表
    const checks = good[index].checks; //获取当前商品的选中状态
    good[index].checks = !checks //改变当前商品的状态
    var all_shop_1 = parseInt(good[index].cart_id) //获取商品的购物车id
    var shop_quantity_1 = good[index].quantity //获取商品的数量
    this.setData({
      dataSource: good
    });
    this.getTotalPrice() //重新获取总价格
    /*设置全选 */
    for (let i = 0; i < good.length; i++) {
      a += good[i].quantity * good[i].price
    }
    if (this.data.total_all_price == a) {
      this.setData({
        checked: true
      })
    } else {
      this.setData({
        checked: false
      })
    }
  },
  /**计算总价格  */
  getTotalPrice() {
    let good = this.data.dataSource; // 获取购物车列表
    let total = 0;
    for (let i = 0; i < good.length; i++) { // 循环列表得到每个数据
      if (good[i].checks) { // 判断选中才会计算价格
        total += good[i].quantity * good[i].price; // 所有价格加起来
      }
    }
    this.setData({ // 最后赋值到data中渲染到页面
      good: good,
      total_all_price: total.toFixed(2),
    });
  },
})