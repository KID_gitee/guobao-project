// pagse/Cart/Cart.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    total_price: 25,
    goods:[{
      img: '/image/tea0.png',
      goods_name: '蜜桃水果茶',
      goods_type: 'Fruit-Tea',
      goods_price: 12,
      goods_num: 1,
    },
    {
      img: '/image/fish0.png',
      goods_name: '龟苓膏水果捞',
      goods_type: 'Fruit-Fishing',
      goods_price: 13,
      goods_num: 1,
    },
    {
      img: '/image/fruit_platter.png',
      goods_name: '小份水果拼盘',
      goods_type: 'Fruit-Platter',
      goods_price: 13,
      goods_num: 1,
    }
    ]
  },

  // 添加商品数量，原理同减少商品数量方法一样
  add_num:function(e){
    console.log(e.target.id)
    for (let index = 0; index < this.data.goods.length; index++) {
      const element = this.data.goods[index];
      if (element.goods_name == e.target.id) {
        //索引是动态的 则使用下方方式
        var  goods_num = 'goods['+ index +'].goods_num';
        var new_num = element.goods_num + 1
        this.setData({
          [goods_num]: new_num
        })
        this.get_tatol_price()
        break
      }
    }
  },

  // 减少商品数量
  del_num:function(e){
    console.log(e.target.id)
    // 遍历商品表通过传入的商品名查找要操作的商品
    for (let index = 0; index < this.data.goods.length; index++) {
      const element = this.data.goods[index];
      if (element.goods_name == e.target.id) {
        //setData对象是数组的话而且索引是动态的 则使用下方方式
        var  goods_num = 'goods['+ index +'].goods_num';
        var new_num = element.goods_num - 1
        // 当商品num为0时即移除购物车内该商品
        if (new_num == 0) {
          var that = this
          // 移除商品时弹框询问用户是否确认移除
          wx.showModal({
            title:'移除商品',
            content:'确认移除商品？',
            showCancel:true,
            // 用户点击确定就更新购物车内商品表
            success(res){
              if(res.confirm){
                // 在购物车内从商品编号为index开始，移除1个商品，此时that.data.goods内将移除下标为index的商品，即that.data.goods为移除后的商品表
                that.data.goods.splice(index,1)
                // 更新现在购物车内商品表为移除后的商品表
                var goods = that.data.goods
                that.setData({
                  goods: goods
                })
                that.get_tatol_price()
              }
            }
          })
          break
        }
        this.setData({
          [goods_num]: new_num
        })
        this.get_tatol_price()
        break
      }
    }
  },

  get_tatol_price:function(){
    // 根据购物车里的商品更新总价
    console.log('开始计算购物车内总价')
    var total_price = 0
    for (let index = 0; index < this.data.goods.length; index++) {
      const element = this.data.goods[index];
      total_price = element.goods_price * element.goods_num + total_price
    }
    this.setData({
      total_price: total_price
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.get_tatol_price()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})