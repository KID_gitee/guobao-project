var app = getApp();
Page({
  data: {
    
  },
  onLoad() {
    wx.hideHomeButton()
  },

  getPhoneNumber (e) {
    // console.log(e)
    //调出前面微信授权界面储存起来的openID
    var openId = wx.getStorageSync('openId')
    var session_key = wx.getStorageSync('session_key')
    // console.log(openId)
    wx.request({
      url: getApp().globalData.server + "/index.php/Home/GuoBao/getPhoneNumber",
      header: {"Content-Type": "application/x-www-form-urlencoded" },
      method: "post",
      data: {
        encryptedData: e.detail.encryptedData, iv: e.detail.iv, session_key:session_key, openId:openId
      },
      success:function(res){
        console.log(res);
        if (res.data.msg.phoneNumber){
          wx.setStorageSync('phoneNumber', res.data.msg.phoneNumber);
          setTimeout(function(){
            wx.reLaunch({
              url: '/pages/home/home',
            })
          },200);
        }else{
          wx.showToast({
            title: '授权失败',
            icon:'loading'
          })
        }
      },
      fail:function(){
        wx.showToast({
          title: '授权失败',
          icon: 'loading'
        })
      }
    })
  }

})