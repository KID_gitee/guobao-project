const app = getApp()
Page({
  data: {
    hasUserInfo: false,
    canIUseGetUserProfile: false,
  },
  onLoad() {
    wx.hideHomeButton()
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '用于个人资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        wx.login({
          success: (res) => {
              // 通过code换取openid
              if (res.code) {
                  console.log(res.code)
                  wx.request({
                      url: getApp().globalData.server + "/index.php/Home/GuoBao/getOpenid",
                      method: "post",
                      data: {
                          code: res.code,
                      },
                      header: {
                        'content-type': "application/x-www-form-urlencoded"   //注意，这里得用这个header！！
                      },
                      success: (res) => {    
                        console.log(res)     
                        console.log('获取到的用户openid为：' + res.data.openid);
                        if (res.data && res.data.openid) {
                            // 获取的openid存入storage，方便之后使用
                            wx.setStorageSync("openId", res.data.openid);
                        }
                        if (res.data && res.data.session_key) {
                          // 获取的session_key存入storage，方便之后使用
                          wx.setStorageSync("session_key", res.data.session_key);
                          console.log('获取到的用户session_key为：' + res.data.session_key);
                      }
                      },
                  });
              }
          },
          fail: () => {},
          complete: () => {
            app.globalData.userInfo = res.userInfo
            wx.setStorageSync("userInfo", res.userInfo);
            // console.log(app.globalData.userInfo)
            this.setData({
              hasUserInfo: true
            })
            wx.reLaunch({
              url: '/pages/auth2/auth2',
            })
          },
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
})