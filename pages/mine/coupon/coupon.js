// pages/mine/coupon/coupon.js
Page({

    /**
   1. 页面的初始数据
     */
    data: {

    },
    radioChange(e) {
      console.log('radio发生change事件，携带value值为：', e.detail.value)
  
      const items = this.data.items
      for (let i = 0, len = items.length; i < len; ++i) {
        items[i].checked = items[i].value === e.detail.value
      }
  
      this.setData({
        items
      })
    },
    /**
   2. 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
  
    },
  
    /**
   3. 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
  
    },
  
    /**
   4. 生命周期函数--监听页面显示
     */
    onShow: function () {
  
    },
  
    /**
   5. 生命周期函数--监听页面隐藏
     */
    onHide: function () {
  
    },
  
    /**
   6. 生命周期函数--监听页面卸载
     */
    onUnload: function () {
  
    },
  
    /**
   7. 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
  
    },
  
    /**
   8. 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
  
    },
  
    /**
   9. 用户点击右上角分享
     */
    onShareAppMessage: function () {
  
    }
  })
  