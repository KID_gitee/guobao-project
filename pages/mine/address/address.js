// pages/mine/address/address.js

 
var app = getApp()
Page({
  /**
    */
  /**
   * 页面的初始数据
   */
  data: {
    consigneeName: "",                        //收货人姓名
    phone: "",                                //电话号码
    detailedAddress: "",                      //具体地址
    labelList: ["宿舍", "教学楼"],            //标签
    is_default: 0
  },
  consigneeNameInput: function(e) {
    
    this.setData({
      consigneeName: e.detail.value
    })
  },
  phoneInput: function(e) {
    
    this.setData({
      phone: e.detail.value
    })
  },
  detailedAddressInput: function (e) {
    this.setData({
      detailedAddress: e.detail.value
    })
  },
  chooseLabelSelect: function(e) {
    var index = e.currentTarget.dataset.index;
    this.setData({
      labelDefault: index
    })
    console.log(this.data.labelList[this.data.labelDefault])
  },
  submit: function() {
    var consigneeName = this.data.consigneeName;
    console.log(consigneeName)
    var phone = this.data.phone;
    console.log(phone)
    var detailedAddress = this.data.detailedAddress
    console.log(detailedAddress)
    if (consigneeName == "") {
      wx: wx.showToast({
        title: '请输入姓名',
        icon:"error"     
      })
      return false
    }
    else if (phone == "") {
      wx: wx.showToast({
        title: '请输入手机号码',
        icon:"error"    
      })
      return false
    }
    else if (!(/^[1][3,4,5,6.7,8,9][0-9]{9}$/.test(phone))) {
      wx.showToast({
        title: "手机号有误",
        icon:"error"
      })
      return false
    }
    else if (detailedAddress == "") {
      wx: wx.showToast({
        title: '请输入收货地址',
        icon:"error"
        
      })
      return false
    }
    else {
      this.subAddress()     //调用发送地址给数据库接口
    } 
  },

  changeradio:function(e){
    var is_default = this.data.is_default
    this.setData({
      is_default: !is_default
    })
    console.log(this.data.is_default)
  },

  //提交地址信息接口
  subAddress:function(){
    var name = this.data.consigneeName
    var address = this.data.detailedAddress
    var phone = this.data.phone
    var lable = this.data.labelList[this.data.labelDefault]
    var is_default = this.data.is_default
    console.log(wx.getStorageSync('openId'))
    console.log(name)
    wx.request({
      url: getApp().globalData.server + "/index.php/Home/GuoBao/subAddress",
      header: {"Content-Type": "application/x-www-form-urlencoded" },
      method: "post",
      data: {
        name: name,
        address: address,
        phone: phone,
        lable: lable,
        is_default: is_default,
        user_id: wx.getStorageSync('openId')
      },
      success:function(res){
        console.log(res)
      },
      fail:function(){
        
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
 
  },
 
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'linear',
    })
    this.animation = animation
  },
 
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
 
  },
 
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
 
  },
 
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
 
  },
 
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
 
  },
 
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
 
  }
})