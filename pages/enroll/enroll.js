// pages/enroll/enroll.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        username:"",
        phonenumber:"",
        password:"",
        passwordack:"",
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },

    signin:function(e){
        wx.navigateBack({
            delta: 1,
         })
       },
     
      regist:function(e){
        var that = this
        var myreg = /^((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8}$/;
        if(that.data.username == ""){
          wx.showModal({
            title: '提示!',
            content: '请输入用户名！',
            showCancel:false,
            success (res) {}
           })
        }else if(that.data.phonenumber.length == '0'){
         console.log(that.data.phonenumber)
         wx.showModal({
           title: '提示!',
           content: '请输入手机号！',
           showCancel:false,
           success (res) {}
          })
         }else if(that.data.phonenumber.length !== 11){
             wx.showModal({
               title: '提示!',
               content: '手机号长度有误，请重新输入！',
               showCancel:false,
               success (res) {}
              })
        }else if(!myreg.test(that.data.phonenumber)){
           wx.showModal({
             title: '提示!',
             content: '请输入正确的手机号！',
             showCancel:false,
             success (res) {}
            })
        }else if(that.data.password == ""){
           wx.showModal({
             title: '提示!',
             content: '请输入密码！',
             showCancel:false,
             success (res) {}
            })
          }else if(that.data.passwordack == ""){
           wx.showModal({
             title: '提示!',
             content: '请输入确认密码！',
             showCancel:false,
             success (res) {}
            })
          }else if(that.data.passwordack == that.data.passwordack){
           wx.showModal({
             title: '提示!',
             content: '两次输入密码不一致！',
             showCancel:false,
             success (res) {}
            })
          }else{
           console.log("success")
          }
     },
     
        usernameInput:function(e){
         this.data.username = e.detail.value
     },
       phonenumberInput:function(e){
         this.data.phonenumber = e.detail.value
     },
       passwordInput:function(e){
         this.data.passwordInput = e.detail.value
     },
       passwordInputAck:function(e){
         this.data.passwordInputAck = e.detail.value
     },
     
       doRegist:function(e){
         var formObject= e.detail.value;
         var username=formObject.username;
         var password=formObject.password;
         if(username.length==0||password.length==0){
             wx.showToast({
               title: '用户名或密码不能为空',
               icon:"none",
               duration:3000
             })
         }else{
           var serverUrl=app.serverUrl;
           wx.request({
             url: serverUrl +'/regist',
             method:'POST',
             data:{
               username:username,
               password:password
             },
             header:{
               'content-type': 'application/json'
             },
             success:function(res){
               console.log(res.data)
               if (res.data.status==200){
                   wx.showToast({
                     title: '恭喜你，注册成功',
                     icon:"none",
                     duration:3000
                   })
               }else{
                 wx.showToast({
                   title: res.data.msg,
                   icon:"none",
                   duration:3000
                 })
               }
     
             }
           })
         }
       },
       goLoginPage:function(){
         wx.navigateTo({
           url: '/pages/login/login'
         })
       }
})